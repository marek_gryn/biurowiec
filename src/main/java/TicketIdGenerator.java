/**
 * Created by Marek on 31.08.2017.
 */
public class TicketIdGenerator {
    private static TicketIdGenerator ourInstance = new TicketIdGenerator();
    private static Integer id=0;
    public static TicketIdGenerator getInstance() {
        return ourInstance;
    }



    private TicketIdGenerator() {
    }

    public static  Integer returnNewTicketId (){
        id++;
        return id;
    }
}
