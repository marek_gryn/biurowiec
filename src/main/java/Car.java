/**
 * Created by Marek on 31.08.2017.
 */
public class Car {
    private Integer ticketId;
    private String registrationNumber;

    public Car(Integer ticketId, String registrationNumber) {
        this.ticketId = ticketId;
        this.registrationNumber = registrationNumber;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }
}
