import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marek on 31.08.2017.
 */
public class Parking {
    private Integer capacity;
    private String name;
    private List<Ticket> tickets=new ArrayList<Ticket>();

    public Parking(Integer capacity, String name) {
        this.capacity = capacity;
        this.name = name;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addTicketToList(Ticket ticket){
        if (tickets.size()<capacity){
            tickets.add(ticket);
        }
        else{
            System.out.println("Brak miejsc parkingowych");
        }

    }

    public Ticket getTicketById(Integer id){
        return tickets.get(id);
    }

    public Boolean isFull(){
        if (capacity==tickets.size()){
            return true;
        }else{
            return false;
        }
    }

}
