/**
 * Created by Marek on 31.08.2017.
 */
public class Ticket {
    private Integer id;
    private Long timestampIn;
    private Long timestampOut;
    private Long timestampPay;
    private Boolean paid;
    private Double amountToPay;
    private Double converterInfo;
    private String registrationNumber;



    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Ticket() {
        this.id = TicketIdGenerator.returnNewTicketId();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getTimestampIn() {
        return timestampIn;
    }

    public void setTimestampIn(Long timestampIn) {
        this.timestampIn = timestampIn;
    }

    public Long getTimestampOut() {
        return timestampOut;
    }

    public void setTimestampOut(Long timestampOut) {
        this.timestampOut = timestampOut;
    }

    public Long getTimestampPay() {
        return timestampPay;
    }

    public void setTimestampPay(Long timestampPay) {
        this.timestampPay = timestampPay;
    }

    public Boolean getPaid() {
        return paid;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    public Double getAmountToPay() {
        return amountToPay;
    }

    public void setAmountToPay(Double amountToPay) {
        this.amountToPay = amountToPay;
    }

    public Double getConverterInfo() {
        return converterInfo;
    }

    public void setConverterInfo(Double converterInfo) {
        this.converterInfo = converterInfo;
    }

    @Override
    public String toString() {
        return "Ticket " +
                "id=" + id +
                ", timestampIn=" + timestampIn +
                ", timestampOut=" + timestampOut +
                ", timestampPay=" + timestampPay +
                ", paid=" + paid +
                ", amountToPay=" + amountToPay +
                ", converterInfo=" + converterInfo +
                '}';
    }
}
