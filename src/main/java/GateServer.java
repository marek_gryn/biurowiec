import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Marek on 31.08.2017.
 */
public class GateServer {
    private List<Car> CarList= new ArrayList<Car>();
    private Map<String, Parking>parkings=new HashMap<String, Parking>();
    private Map<Gate,String> gate2parking=new HashMap<Gate, String>();

    public void addParking(String name, Integer capacity){
        parkings.put(name,new Parking(capacity,name));
    }

    public void addGate(Integer gateId, String parkingName){
        gate2parking.put(new Gate(gateId,this),parkingName);
    }



    public Ticket generateTicket(Gate gate){
        if(parkings.get(gate2parking.get(gate)).isFull()){
            System.out.println("Parking jest pełen");
            return null;
        }
        else{
            Ticket ticket=new Ticket();
            return ticket;
        }
    }

    public void addTicket (Ticket ticket, Gate gate){
        parkings.get(gate2parking.get(gate)).addTicketToList(ticket);
    }


}
