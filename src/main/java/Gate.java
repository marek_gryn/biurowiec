import java.util.Optional;

/**
 * Created by Marek on 31.08.2017.
 */
public class Gate {
    private Integer gateId;
    private Boolean isOpen;
    private GateServer gateServer;



    public Gate(Integer gateId,GateServer gateServer) {
        this.gateId=gateId;
        this.isOpen = true;
        this.gateServer = gateServer;

    }

    public Optional<Ticket> generateTicket(String registrationNumber) {

        Ticket ticket=gateServer.generateTicket(this);
        return Optional.of(ticket);
    }

    public Boolean chceckIn(String registrationNumber) {

        Optional<Ticket> ticket = this.generateTicket(registrationNumber);
        if (!ticket.equals(Optional.empty())) {
            System.out.println(ticket.toString());

            return true;
        }else{
            return false;
        }
    }
}




